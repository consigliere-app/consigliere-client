export enum GameResult {
  MAFIA = 'mafia',
  CIVILIANS = 'civilians',
  TIE = 'tie',
}
