import { RoundPhase } from '@/table/models/day-phase.enum';

export interface QuitPhase {
  stage: RoundPhase;
  number: number;
}
