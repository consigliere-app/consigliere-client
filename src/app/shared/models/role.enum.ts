export enum Role {
  MAFIA = 'Мафия',
  CITIZEN = 'Мирный',
  DON = 'Дон',
  SHERIFF = 'Шериф',
  HOST = 'Ведущий',
}
